# flask

To run the app, make sure you have flask and wtform install in conda3

To install flask on Linux: 
	$ sudo apt-get install python-virtualenv
For all other Operating System, please follow the link below:
http://flask.pocoo.org/docs/0.12/installation/

To install wtform on Linux:
	$ conda install -c anaconda3 wtforms	

1. cd into the directory and do: python3 app.py

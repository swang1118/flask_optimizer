from flask import Flask, render_template, request
from wtforms import Form, IntegerField, FloatField, validators
#############################
import pdb
import matplotlib
import matplotlib.pyplot as plt
import sys
import math
import numpy as np
import pandas as pd
import os
import time
import pickle
import random
from scipy.stats import norm
import argparse
import shlex
import re
from scipy import interpolate
import shutil
import sklearn
import sklearn.metrics
import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.parallel
import torchvision
import torchvision.transforms as transforms
import numpy as np

from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
# load custom functions
sys.path.append('/home/pacrim/git/pacrim/lib/')
from pacrim_utils import *


class Net(nn.Module):

    def __init__(self, net_dict):
        super(Net, self).__init__()

        self.fc1 = nn.Linear(net_dict['n_feature'], net_dict['n_hidden'])
        self.fc2 = nn.Linear(net_dict['n_hidden'], net_dict['n_hidden'])
        self.fc3 = nn.Linear(net_dict['n_hidden'], net_dict['n_output'])
        self.fc4 = nn.Linear(net_dict['n_hidden'], net_dict['n_output'])

    def forward(self, x):
        x = F.sigmoid(self.fc1(x))
        x = F.sigmoid(self.fc2(x))

        y1 = self.fc3(x)
        y2 = F.relu(self.fc4(x)) + 0.1

        x = torch.cat((y1,y2), dim=1)
        return x

# figure out how to pass in the number of hidden layers.
net1_dict={'n_feature': 5, 'n_hidden': 20, 'n_output': 1 }
net1 = Net(net1_dict)

net2_dict={'n_feature': 3, 'n_hidden': 20, 'n_output': 1 }
net2=Net(net2_dict)

def load_obj(name, path ):
    with open(path + name + '.pkl', 'rb') as f:
        return pickle.load(f)


# load the model which takes specific duration and the string path of where the models are located.
def load_model(duration, path_str, net):
    model = net.cuda()

    ## parse duration into a string and load the corresponding model file
    #checkpoint = torch.load('/home/swang/flask/model_best.pth.tar')
    model = torch.nn.DataParallel(model).cuda()
    checkpoint = torch.load(path_str.format(duration))
    best_prec = checkpoint['best_prec']
    model.load_state_dict(checkpoint['state_dict'])
    return model

#model = load_model(7)

def predict_return_stat(model, DTE, OTM, VIX, FV, SPY_pct_chg, duration):
    """
    input:  model
            DTE - time to expiration in days
            OTM - out-of-the-money in percentage
            VIX - current VIX
            FV -  VIX after one week
    """
    # preprocessing
    
    # loading the pickle file for all the normalization numbers
    norm_dict = load_obj('normalization{}'.format(duration),'/mnt/disk2/durations_folder/duration_norms/')
    DTE_norm = DTE/250
    OTM_norm = OTM/norm_dict['otm']
    VIX_norm = (VIX - norm_dict['VIX_mean'])/norm_dict['VIX_std']
    FV_norm = np.log(FV/VIX)
    SPY_pct_chg_norm = SPY_pct_chg/norm_dict['SPY_pct_change'] 
    
    X = np.array([DTE_norm, OTM_norm, VIX_norm, FV_norm, SPY_pct_chg_norm]).astype(np.float32)
    #X = np.array([DTE/250, OTM/50, (VIX-17.615)/5.672911, np.log(FV/VIX), SPY_pct_chg]).astype(np.float32)
    X = X.reshape(1,5)

    y = model(Variable(torch.FloatTensor(X).cuda()))
    #pdb.set_trace()
    y = y.data.cpu().numpy()
    return y * norm_dict['return']

# predict premium either normalized or not. norm argument is either 1(not normalized) or 2(normalized)
def predict_premium(model, DTE, OTM, VIX, norm):
    norm_dict = load_obj('normalization{}'.format(norm), '/mnt/disk2/bs_compare/duration_norms/')
    DTE_norm = DTE/250
    OTM_norm = OTM/norm_dict['otm']
    VIX_norm = (VIX - norm_dict['VIX_mean'])/norm_dict['VIX_std']
    
    X = np.array([DTE_norm, OTM_norm, VIX_norm]).astype(np.float32)
    X = X.reshape(1,3)

    y = model(Variable(torch.FloatTensor(X).cuda()))
    #pdb.set_trace()
    y = y.data.cpu().numpy()
    return y * norm_dict['p']

# find the best otm dte combination.
def find_best_otm_dte(VIX, FV):
    best = -1e17
    curr_dte = 0
    curr_otm = 0
    # i for DTE
    for i in range(2, 250, 5):
        # j for OTM
        for j in range(2, 20):
            curr_re = predict_return_stat(model, i, j, VIX, FV)[0][0]
            if curr_re > best:
                best = curr_re
                curr_dte = i
                curr_otm = j
    return (curr_dte, curr_otm, best)


#############################
app = Flask(__name__)

# Model
class ReturnForm(Form):
    c = FloatField('Current VIX', validators=[validators.InputRequired()])
    f = FloatField('Future VIX', validators=[validators.InputRequired()])
    dte = FloatField('DTE (in Days)', validators=[validators.InputRequired()])
    otm = FloatField('OTM (in Percentage)', validators=[validators.InputRequired()])
    duration = IntegerField('Duration between VIX and Future VIX (in Days)', validators=[validators.InputRequired()])
    spy_pct_chg = FloatField('SPY Change (in Decimal)', validators=[validators.InputRequired()])
    current_option_premium = FloatField('Current Option Premium', validators=[validators.InputRequired()])
    strike_price = FloatField('Strike Price', validators=[validators.InputRequired()])

class BestForm(Form):
    c = FloatField('cvix', validators=[validators.InputRequired()])
    f = FloatField('fvix', validators=[validators.InputRequired()])

class PremiumForm(Form):
    c = FloatField('Current VIX', validators=[validators.InputRequired()])
    dte = FloatField('DTE (in Days)', validators=[validators.InputRequired()])
    otm = FloatField('OTM (in Percentage)', validators=[validators.InputRequired()])
    f = FloatField('Future VIX', validators=[validators.InputRequired()])
    dte2 = FloatField('DTE2 (in Days)', validators=[validators.InputRequired()])
    otm2 = FloatField('OTM2 (in Percentage)', validators=[validators.InputRequired()])

#View
@app.route('/', methods=['GET', 'POST'])
def index():
    form = ReturnForm(request.form)
    if request.method == 'POST' and form.validate():
        c = form.c.data
        f = form.f.data
        dte = form.dte.data
        otm = form.otm.data
        duration = form.duration.data
        spy_pct_chg = form.spy_pct_chg.data
        current_option_premium = form.current_option_premium.data
        strike_price = form.strike_price.data
        
        path_mean_return = '/mnt/disk2/durations_folder/tars/{}.pth.tar'

        model_mean_return = load_model(duration, path_mean_return, net1)
          
        #pdb.set_trace()
    
        (mean_return, std_dev)= predict_return_stat(model_mean_return, dte, otm, c, f, spy_pct_chg, duration)[0]
        real_mean_return = mean_return * strike_price
        n = 252/duration
        mean_annualized_return = (1+mean_return)**n-1
        real_mean_annualized_return = mean_annualized_return * strike_price 
        future_option_premium=current_option_premium-mean_return * strike_price 
    else:
        mean_return = None
        real_mean_return = None
        std_dev = None
        mean_annualized_return = None
        real_mean_annualized_return = None
        annualized_std_dev = None
        future_option_premium = None
        c = None
        f = None
        dte = None
        otm = None
        duration = None
        spy_pct_chg = None
    return render_template('home.html', form=form,\
                                        mean_annualized_return=mean_annualized_return, \
                                        real_mean_annualized_return=real_mean_annualized_return,\
                                        mean_return=mean_return,\
                                        real_mean_return=real_mean_return,\
                                        std_dev=std_dev, \
                                        future_option_premium = future_option_premium, \
                                        c=c, f=f, dte=dte, otm=otm, \
                                        duration=duration, \
                                        spy_pct_chg=spy_pct_chg)

@app.route('/premium', methods=['GET', 'POST'])
def premium():
    form = PremiumForm(request.form)
    if request.method == 'POST' and form.validate():
        c = form.c.data
        dte = form.dte.data
        otm = form.otm.data
        f = form.f.data
        dte2 = form.dte2.data
        otm2 = form.otm2.data
        path_norm_p = '/mnt/disk2/bs_compare/model.pth.tar'

        path_p = '/mnt/disk2/bs_compare/Checkpoint_LL/duration1/ckpt_epoch3.tar'
        # load the model for predicting premium
        model_norm_p = load_model('', path_norm_p, net2)
        model_p = load_model('', path_p, net2)
        (p1_norm, std_dev_norm1) = predict_premium(model_norm_p, dte, otm, c, 2)[0]
        (p2_norm, std_dev_norm2) = predict_premium(model_norm_p, dte2, otm2, f, 2)[0]
        
        (p1, std_dev1) = predict_premium(model_p, dte, otm, c, 1)[0]
        (p2, std_dev2) = predict_premium(model_p, dte2, otm2, f, 1)[0]
        
    else:
        p1_norm = None
        p2_norm = None
        p1 = None
        p2 = None
    return render_template('premium.html', form=form, p1_norm=p1_norm, p2_norm=p2_norm, p1=p1, p2=p2, std_dev_norm1=std_dev_norm1, std_dev_norm2=std_dev_norm2, std_dev1=std_dev1, std_dev2=std_dev2)



@app.route('/best', methods=['GET', 'POST'])
def best():
    form = BestForm(request.form)
    if request.method == 'POST' and form.validate():
        c = form.c.data
        f = form.f.data
        best_dte = find_best_otm_dte(c, f)[0]
        best_otm = find_best_otm_dte(c, f)[1]
        best_re = find_best_otm_dte(c, f)[2]
        print(c, f, best_dte, best_otm, best_re)
    else:
        best_dte = None
        best_otm = None
        best_re = None
    return render_template('best.html', form=form, best_dte=best_dte, best_otm=best_otm, best_re=best_re)

if __name__ == '__main__':
    app.run(debug=True)

import numpy as np
import os
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import pickle
plt.switch_backend('agg')

def data_stats(data, cols):
    # Calculate statistics for each column in the data
    stats = []
    df = pd.DataFrame(data, columns=cols)
    for col in cols:
        stats = []
    return np.array(stats)

def column_hist(column, save_loc):
    plt.ioff()
    fig = plt.figure()
    plt.hist(column)
    plt.savefig(save_loc)
    plt.close(fig)
    
#data_loc = '/mnt/disk2/durations_folder/training_data/1.npy'
#columns = ['dte', 'otm', 'VIX', 'FV', 'SPY_pct_change', 'return']
#data = np.load(data_loc)
#for col_index in range(0, data.shape[1]):
#   column_hist(data[:,col_index], columns[col_index]) 

# Saves OBJ to file NAME as pickle
def save_obj(obj, name, path):
    with open(path + name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

# Load obj from pickle file called NAME
def load_obj(name, path):
    with open(path + name + '.pkl', 'rb') as f:
        return pickle.load(f)

def normalize(data, cols):
    # Check if normalized data already created
    local_data = 'local_norm_data'
   # if args.resume != '' and os.path.isfile(local_data):
        # If resuming and local copies exist then use local copies and return
   #     data = np.load(local_data)
   #     n = np.int(data.shape[0]*train_perc)
   #     index = np.random.permutation(data.shape[0])
        # Return the tuple (TRAIN_DATA, TEST_DATA)
   #     return (data[index[:n],:], data[index[n:],:])

    df = pd.DataFrame(data, columns=cols)
    # Dictionary to save that will be used in predicting
    norm_dict = {'otm': df['otm'].max(), 'VIX_mean': df['VIX'].mean(), 'VIX_std': df['VIX'].std(), 'return': df['return'].max(), 'SPY_pct_change': df['SPY_pct_change'].max()}
   # save_obj(norm_dict, 'normalization')
    df['FV'] = (df['FV']/df['VIX']).apply(np.log)
    
    df['dte'] = df['dte']/252
    df['otm'] = df['otm']/norm_dict['otm']
    df['VIX'] = (df['VIX']-norm_dict['VIX_mean'])/norm_dict['VIX_std']
    # Normalizing SPY_pct_change
    df['SPY_pct_change'] = df['SPY_pct_change']/norm_dict['SPY_pct_change']

    #df['return'] = df['return'].apply(lambda x: min(x,  1))  # max return
    #df['return'] = df['return'].apply(lambda x: max(x, -1))  # min return
    df['return'] = df['return']/norm_dict['return']
    df = df.dropna(axis=0, how='any')

    data = df[cols].values
    return data

def graph(x, y, name):
    plt.ioff()
    x_axis = np.array(range(1, y.shape[0]))
    fig = plt.figure()
    # Plot the graphs
    plt.plt(x_axis, x, 'r--', x_axis, y, 'g^')
    # Label the graph
    plt.xlabel("count")
    plt.ylabel(name)
    plt.title(name)
    plt.savefig(save_loc)
    plt.close(fig)

#norm_data = normalize(data, columns)
#graph(norm_data['otm'], norm_data['dte'], 'plot otm')   
    
#for col_index in range(0, norm_data.shape[1]):
#    column_hist(norm_data[:,col_index], columns[col_index]+'other') 

import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.parallel
import torchvision
import torchvision.transforms as transforms
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
# torch related above
import numpy as np
import os
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import pickle
import p1
from p1 import *
plt.switch_backend('agg')

# load the model which takes specific duration and the string path of where the models are located.
def load_model(duration, path_str):
    model = Net().cuda()

    ## parse duration into a string and load the corresponding model file
    #checkpoint = torch.load('/home/swang/flask/model_best.pth.tar')
    #model = torch.nn.DataParallel(model).cuda() 
    checkpoint = torch.load(path_str.format(duration))
    best_prec = checkpoint['best_prec']
    model.load_state_dict(checkpoint['state_dict'])
    return model

#def predict_return_stat(model, data):
#    """
#    input:  model
#            DTE - time to expiration in days
#            OTM - out-of-the-money in percentage
#            VIX - current VIX
#            FV -  VIX after one week
#    """
#    # preprocessing
#    X = np.array([data[:, 0]/250, data[:,1]/50, (data[:, 2]-17.615)/5.672911, np.log(data[:, 3]/data[:, 2]), data[:, 4]]).astype(np.float32).T
#    if X.shape[1] is None:
#        X = X.reshape(1,5)
#
#    y = model(Variable(torch.FloatTensor(X).cuda()))
#    #pdb.set_trace()
#    y = y.data.cpu().numpy()
#    return y[:, 0] *.29

# find the best otm dte combination.
def find_best_otm_dte(VIX, FV):
    best = -1e17
    curr_dte = 0
    curr_otm = 0
    # i for DTE
    for i in range(2, 250, 5):
        # j for OTM
        for j in range(2, 20):
            curr_re = predict_return_stat(model, i, j, VIX, FV)[0][0]
            if curr_re > best:
                best = curr_re
                curr_dte = i
                curr_otm = j
    return (curr_dte, curr_otm, best)

####################################################################################

def data_stats(data, cols):
    # Calculate statistics for each column in the data
    stats = []
    df = pd.DataFrame(data, columns=cols)
    for col in cols:
        stats = []
    return np.array(stats)

def column_hist(column, save_loc):
    plt.ioff()
    fig = plt.figure()
    plt.hist(column)
    plt.savefig(save_loc)
    plt.close(fig)
    
# Saves OBJ to file NAME as pickle
def save_obj(obj, name ):
    with open('obj/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

# Load obj from pickle file called NAME
def load_obj(name, path ):
    with open(path + name + '.pkl', 'rb') as f:
        return pickle.load(f)

def normalize(data, cols):
    # Check if normalized data already created
    local_data = 'local_norm_data'
   # if args.resume != '' and os.path.isfile(local_data):
        # If resuming and local copies exist then use local copies and return
   #     data = np.load(local_data)
   #     n = np.int(data.shape[0]*train_perc)
   #     index = np.random.permutation(data.shape[0])
        # Return the tuple (TRAIN_DATA, TEST_DATA)
   #     return (data[index[:n],:], data[index[n:],:])

    df = pd.DataFrame(data, columns=cols)
    # Dictionary to save that will be used in predicting
    norm_dict = {'otm': df['otm'].max(), 'VIX_mean': df['VIX'].mean(), 'VIX_std': df['VIX'].std(), 'return': df['return'].max(), 'SPY_pct_change': df['SPY_pct_change'].max()}
   # save_obj(norm_dict, 'normalization')
    df['FV'] = (df['FV']/df['VIX']).apply(np.log)
    
    df['dte'] = df['dte']/252
    df['otm'] = df['otm']/norm_dict['otm']
    df['VIX'] = (df['VIX']-norm_dict['VIX_mean'])/norm_dict['VIX_std']
    # Normalizing SPY_pct_change
    df['SPY_pct_change'] = df['SPY_pct_change']/norm_dict['SPY_pct_change']

    #df['return'] = df['return'].apply(lambda x: min(x,  1))  # max return
    #df['return'] = df['return'].apply(lambda x: max(x, -1))  # min return
    df['return'] = df['return']/norm_dict['return']
    df = df.dropna(axis=0, how='any')

    data = df[cols].values
    return data

def graph(x, y, name):
    plt.ioff()
    x_axis = np.array(range(1, y.shape[0]+1))
    fig = plt.figure()
    # Plot the graphs
    plt.plot(x_axis, x, 'r^', x_axis, y, 'g^')
    # Label the graph
    plt.xlabel("VIX")
    plt.ylabel(name)
    plt.title(name)
    plt.savefig(name)
    plt.close(fig)


############################################# graph trends ###############################################
def graph_vix(x, y, x_label, y_label, name, path):
    plt.ioff()
    fig = plt.figure()
    # Plot the graphs
    plt.plot(x, y)
    # Label the graph
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(name)
    plt.savefig(path + '/' + name)
    plt.close(fig)

# loading the min max dictionaries for all parameters and the param dictionaries for all the default values for all the params(vix, fv, dte, otm, spy_chg)
def load_values(duration):
    f1 = np.load('/mnt/disk2/durations_folder/training_data/{}.npy'.format(duration))
    f1 = f1[~np.all((f1==0.0), axis=1)]
    dte = f1[:, 0]
    otm = f1[:, 1]
    vix = f1[:, 2]
    fv = f1[:, 3]
    spy_chg = f1[:, 4]
    r = f1[:, 5]
   
    # min max param dictionary
    mm_dict={'min_vix':vix.min(),'max_vix':vix.max(),'min_fv':fv.min(),'max_fv':fv.max(), 'min_otm':otm.min(),'max_otm':otm.max(),'min_dte':dte.min(),'max_dte':dte.max(),'min_spy_chg':spy_chg.min(),'max_spy_chg':spy_chg.max(), 'mean_vix':vix.mean(),'std_vix':np.std(vix),'max_return': r.max()}
    print(np.mean(vix))
    print(np.mean(fv))
    print(np.mean(dte))
    print(np.mean(otm))
    print(np.mean(spy_chg)) 
    vix = np.full((1000), np.mean(vix), dtype=float)
    fv = np.full((1000), np.mean(fv), dtype=float)
    dte = np.full((1000), np.mean(dte), dtype=float)
    otm = np.full((1000), np.mean(otm), dtype=float)
    spy_chg = np.full((1000), np.mean(spy_chg), dtype=float)
   
  #  vix = np.full((1000), np.median(vix), dtype=float)
  #  fv = np.full((1000), np.median(fv), dtype=float)
  #  dte = np.full((1000), np.median(dte), dtype=float)
  #  otm = np.full((1000), np.median(otm), dtype=float)
  #  spy_chg = np.full((1000), np.median(spy_chg), dtype=float)
   
 
    # param default values dictionary
    param_dict={'vix': vix, 'fv':fv, 'dte':dte, 'otm':otm, 'spy_chg':spy_chg}
    return mm_dict, param_dict
    
# getting predictions and return the updated param dictionary
def predict_return_for_param(model, chg_param, duration, param_dict, mm_dict):
    norm_dict = load_obj('normalization{}'.format(duration),'/mnt/disk2/durations_folder/duration_norms/')
    
    param_dict[chg_param] = np.linspace(mm_dict['min_{}'.format(chg_param)], mm_dict['max_{}'.format(chg_param)], 1000)
    DTE_norm = param_dict['dte']/250
    OTM_norm = param_dict['otm']/norm_dict['otm']
    VIX_norm = (param_dict['vix'] - norm_dict['VIX_mean'])/norm_dict['VIX_std']
    FV_norm = np.log(param_dict['fv']/param_dict['vix'])
    SPY_pct_chg_norm = param_dict['spy_chg']/norm_dict['SPY_pct_change']
    #DTE_norm = param_dict['dte']/250
    #OTM_norm = param_dict['otm']/mm_dict['max_otm']
    #VIX_norm = (param_dict['vix'] - mm_dict['mean_vix'])/mm_dict['std_vix']
    #FV_norm = np.log(param_dict['fv']/param_dict['vix'])
    #SPY_pct_chg_norm = param_dict['spy_chg']/mm_dict['max_spy_chg']
    X = np.array([DTE_norm, OTM_norm, VIX_norm, FV_norm, SPY_pct_chg_norm]).astype(np.float32).T
    if X.shape[1] is None:
        X = X.reshape(1,5)

    y = model(Variable(torch.FloatTensor(X).cuda()))
    y = y.data.cpu().numpy()
    return y[:, 0] *norm_dict['return'], param_dict

def plot_trend(key, name, duration, path, param_dict, mm_dict, x_label, y_label):
    predict_return, param_new_dict = predict_return_for_param(model, key, duration, param_dict, mm_dict) 
    graph_vix(param_new_dict[key], predict_return, x_label, y_label, name, path)

duration = 1 
model = load_model(duration, '/mnt/disk2/durations_folder/tars/{}.pth.tar')
mm_dict, param_dict = load_values(duration)
path = "./trend_plots/{}".format(duration) 
plot_trend('vix', 'vix_plot', duration, path, param_dict, mm_dict, 'vix', 'return')

mm_dict, param_dict = load_values(duration)
plot_trend('fv', 'fv_plot', duration, path, param_dict, mm_dict, 'fv', 'return')

mm_dict, param_dict = load_values(duration)
plot_trend('dte', 'dte_plot', duration, path, param_dict, mm_dict, 'dte', 'return')

mm_dict, param_dict = load_values(duration)
plot_trend('otm', 'otm_plot', duration, path, param_dict, mm_dict, 'otm', 'return')

mm_dict, param_dict = load_values(duration)
plot_trend('spy_chg', 'spy_plot', duration, path, param_dict, mm_dict, 'otm', 'return')

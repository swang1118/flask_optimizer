import math
import numpy as np
import pandas as pd
import os
import sys
import time
import pickle
import random
from scipy.stats import norm
import argparse
import shlex
import re
from scipy import interpolate
import shutil
import sklearn
import sklearn.metrics
import torch
from torch.autograd import Variable
import torch.nn as nn
from torch.backends import cudnn
import torch.nn.functional as F
import torch.nn.parallel
import torchvision
import torchvision.transforms as transforms
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
# load custom functions
sys.path.append('/home/pacrim/git/pacrim/lib/')
from pacrim_utils import *
from graph import * 

parser = argparse.ArgumentParser(description='Optimize Option DTE/OTM')
parser.add_argument('--resume', default='', type=str, help='path to latest checkpoint (default: none)')
parser.add_argument('--lr', default='0.01', type=float, help='learning rate (default: 0.01)')
parser.add_argument('--start-epoch', default='0', type=int, help='start epoch (default: 0)')
parser.add_argument('--epochs', default='1', type=int, help='epochs(default: 100)')
#parser.add_argument('--batch-size', default='50', type=float, help='batch size (default: 50)')
parser.add_argument('--momentum', default='0.5', type=float, help='momentum (default: 0.5)')
parser.add_argument('--weight-decay', default='0.', type=float, help='weight decay (default: 0)')
parser.add_argument('--print-freq', default='1000', type=int, help='print frequecy (default: 1000)')
#parser.add_argument('--loss', default='mse', type=str, help='loss function (default: mse)')
parser.add_argument('--duration', default='1', type=str, help='save the filename corresponding to a specific duration (default: 1)')
#parser.add_argument('--loadfile', default='1', type=str, help='load the file *.npy (default: 1)')

class Net(nn.Module):

    def __init__(self):
        super(Net, self).__init__()
       # an affine operation: y = Wx + b
        
        self.fc1 = nn.Linear(5, 20)
        self.fc2 = nn.Linear(20, 20)
        self.fc3 = nn.Linear(20,1)
        self.fc4 = nn.Linear(20,1)
    
    def forward(self, x):
        x = F.sigmoid(self.fc1(x))
        x = F.sigmoid(self.fc2(x))
        #x = torch.cat(x[:,0:1], 0.01+F.relu(x[:,1:2])),dim=1)        
        y1 = self.fc3(x)
        y2 = F.relu(self.fc4(x))+0.1
    
        x = torch.cat((y1,y2), dim=1)            
        return x
# loss function for negative log likelihood
def negLL(output, target):
    return neg_log_normal(target, output[:,0], output[:,1])


# define loss function -log normal 
def neg_log_normal(x, mu, var):
    LL =  (x - mu)**2/(2*var) + Variable.log(var)/2 + np.log(2*np.pi)/2
    return LL.mean()

class MyDataset(Dataset):
    def __init__(self, data):
        self.data = torch.from_numpy(data)

    def __len__(self):
        return len(self.data)
        
    def __getitem__(self, idx):
        return self.data[idx]

# for shuffling data
class MyDataLoader:    
    def __init__(self, data, shuffle=True, batch_size=50):
        if shuffle:
            index = np.random.permutation(data.shape[0])
        else:
            index = np.arange(data.shape[0])
        
        self.i = 0
        self.batch_size = batch_size
        self.data = data
        self.index = index       
        self.n = np.int(np.floor(data.shape[0]/batch_size))
        
    def __iter__(self):
        return self
    
    def reset(self):
        self.i = 0
    
    def __len__(self):
        return self.n
    
    def __next__(self):
        if self.i < self.n:
            i = self.i
            self.i += 1            
            return torch.FloatTensor(self.data[self.index[i*self.batch_size:(i+1)*self.batch_size],:])
        else:
            raise StopIteration()

def train(train_loader, model, criterion, optimizer, epoch):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    scores = AverageMeter()
    mses = AverageMeter()
    
    # switch to train mode
    model.train()

    end = time.time()

    for i, samples in enumerate(train_loader):

        # measure data loading time
        data_time.update(time.time() - end)

        input_var = Variable(samples[:,:5].cuda())
        target_var = Variable(samples[:,-1].cuda())  # last column is the return

        # compute output
        output = model(input_var)#.view(-1)
        #print(output.size())
        loss = criterion(output, target_var)

        # measure accuracy and record loss
        scores.update(accuracy(output.data[:,0], target_var.data), samples.size(0))
        losses.update(loss.data[0], samples.size(0))
        mses.update(mean_squared_error(output.data[:,0], target_var.data), samples.size(0))

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if i % args.print_freq  == 0:
            print('Epoch: [{0}/{1}]\t'
                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                  'MSE {mses.val:.3f} ({mses.avg:.3f})\t'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
                  'Score {score.val:.3f} ({score.avg:.3f})'.format(
                   i, len(train_loader), batch_time=batch_time,
                   mses=mses, loss=losses, score=scores))     
    return losses.avg

def validate(val_loader, model, criterion):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    scores = AverageMeter()
    mses = AverageMeter()
    
    # switch to train mode
    model.eval()

    end = time.time()
    for i, samples in enumerate(val_loader):
         # measure data loading time
        data_time.update(time.time() - end)

        input_var = Variable(samples[:,:5].cuda())
        target_var = Variable(samples[:,-1].cuda())  # last column is the return

        # compute output
        output = model(input_var)#.view(-1)
        loss = criterion(output, target_var)

        # measure accuracy and record loss
        scores.update(accuracy(output.data[:,0], target_var.data), samples.size(0))
        losses.update(loss.data[0], samples.size(0))
        mses.update(mean_squared_error(output.data[:,0], target_var.data), samples.size(0))
        
        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if i % args.print_freq  == 0:
            print('Epoch: [{0}/{1}]\t'
                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                  'MSE {mses.val:.3f} ({mses.avg:.3f})\t'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
                  'Score {score.val:.3f} ({score.avg:.3f})'.format(
                   i, len(val_loader), batch_time=batch_time,
                   mses=mses, loss=losses, score=scores))                
    return losses.avg

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def adjust_learning_rate(optimizer, epoch):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    lr = args.lr * (0.1 ** (epoch // 30))
    for param_group in optimizer.state_dict()['param_groups']:
        param_group['lr'] = lr

def accuracy(y_pred, y_true):
    """Computes r2 """
    return sklearn.metrics.r2_score(y_true.cpu().numpy(), y_pred.cpu().numpy())


def mean_squared_error(y_pred, y_true):
    """Computes mean_square_error """
    return sklearn.metrics.mean_squared_error(y_true.cpu().numpy(), y_pred.cpu().numpy())

def save_checkpoint(state, is_best, checkpoint, directory, filename, duration_str):
    if not os.path.exists(checkpoint):
        os.makedirs(checkpoint)
    if not os.path.exists(checkpoint + '/' + directory):
        os.makedirs(checkpoint + '/' + directory)
    torch.save(state, checkpoint + '/' +  directory + '/' + filename)
    if is_best:
        shutil.copyfile(checkpoint + '/' + directory + '/' + filename, '/mnt/disk2/durations_folder/tars' + duration_str + '.pth.tar')
        shutil.copyfile(checkpoint + '/' + directory + '/' + filename, 'tars' + duration_str + '.pth.tar')

def load_and_preprocess(file, train_perc = 0.7):
    # load data
    local_data_path = '/mnt/disk2/durations_folder/lambda_npy_backups/'
    if os.path.isfile(local_data_path + 'train_data' + args.duration + '.npy'):
        # If resuming and local copies exist then use local copies and return
        train = np.load(local_data_path + 'train_data' + args.duration + '.npy')
        test = np.load(local_data_path + 'test_data' + args.duration + '.npy')
        # Return the tuple (TRAIN_DATA, TEST_DATA)
        return (train, test)

    data = np.load(file)
    # ['dte','otm','VIX','FV','FVhigh', 'FVlow', 'return']
    df = pd.DataFrame(data, columns = ['dte','otm','VIX','FV','SPY_pct_change', 'return'])
    norm_dict = {'otm': df['otm'].max(), 'VIX_mean': df['VIX'].mean(), 'VIX_std': df['VIX'].std(), 'return': df['return'].max(), 'SPY_pct_change': df['SPY_pct_change'].max()}
    save_obj(norm_dict, 'normalization' + args.duration, "/mnt/disk2/durations_folder/duration_norms/")

    df['FV'] = (df['FV']/df['VIX']).apply(np.log)
    
    df['dte'] = df['dte']/250 
    df['otm'] = df['otm']/norm_dict['otm']
    df['VIX'] = (df['VIX']-norm_dict['VIX_mean'])/norm_dict['VIX_std']
    
    # Normalizing SPY_pct_change
    df['SPY_pct_change'] = df['SPY_pct_change']/norm_dict['SPY_pct_change']

    df['return'] = df['return'].apply(lambda x: min(x,  1))  # max return
    df['return'] = df['return'].apply(lambda x: max(x, -1))  # min return
    df['return'] = df['return']/norm_dict['return']
    
    data = df[['dte','otm','VIX','FV','SPY_pct_change','return']].values
    index = np.random.permutation(data.shape[0])
    #index = np.arange(data.shape[0])
    n = np.int(data.shape[0]*train_perc)
    train = data[index[:n], :]
    test = data[index[n:],:]
    np.save(local_data_path + 'train_data' + args.duration, train)
    np.save(local_data_path + 'test_data' + args.duration, test)
    return (train, test)


def main():
    global args
    args = parser.parse_args()


    print('Loading data ....')
    np.random.seed(1)
    (train_data, test_data) = load_and_preprocess(file='/mnt/disk2/durations_folder/training_data/' + args.duration + '.npy')
    #train_data = np.load('training_data.npy')
    #test_data = np.load('test_data.npy')
    train_data = train_data[~np.isnan(train_data).any(axis=1)]
    test_data = test_data[~np.isnan(test_data).any(axis=1)]
    #print(np.isnan(train_data).any())
    print('Done data loading')


    model = Net().cuda()

    # initiliaze fc4 parameters - to var
    torch.nn.init.normal(list(model.fc4.parameters())[0], 0, 0.001)
    torch.nn.init.constant(list(model.fc4.parameters())[1],0.2)
    
    #model = torch.nn.DataParallel(model, device_ids=[0, 1])
    #torch.cuda.set_device(0)
    cudnn.benchmark = True


    #criterion = nn.MSELoss().cuda()
    criterion = negLL 
    optimizer = torch.optim.SGD(model.parameters(), lr = args.lr, momentum=args.momentum)


    train_loader = MyDataLoader(train_data, batch_size=2000, shuffle=False)
    val_loader = MyDataLoader(test_data, batch_size=20000, shuffle=False)

   
   
    best_prec = np.inf
    # optionally resume from a checkpoint
    if args.resume:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            checkpoint = torch.load(args.resume)
            args.start_epoch = checkpoint['epoch']
            best_prec = checkpoint['best_prec']
            model.load_state_dict(checkpoint['state_dict'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(1, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(args.resume))


    # validation performance
    val_loader.reset()
    prec = validate(val_loader, model, criterion)
    print('Val loss: ', prec)

    for epoch in range(args.start_epoch, args.start_epoch + args.epochs):
        adjust_learning_rate(optimizer, epoch)

        # train for one epoch
        train_loader.reset()
        train_loss = train(train_loader, model, criterion, optimizer, epoch)

        # evaluate on validation set
        val_loader.reset()
        prec = validate(val_loader, model, criterion)

        # remember best prec@1 and save checkpoint
        is_best = prec < best_prec
        best_prec = min(prec, best_prec)
        checkpoint = '/mnt/disk2/durations_folder/durations_checkpoints/Checkpoint_LL'
        directory = 'duration' + args.duration
        filename = 'ckpt_epoch{:d}.tar'.format(epoch)
        save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'best_prec': best_prec
        }, is_best, checkpoint , directory, filename, args.duration)

        print('Epoch: {:d}  Train Loss: {:f} Val Loss: {:f}'.format(epoch, train_loss, prec))



if __name__ == '__main__':
    main()

